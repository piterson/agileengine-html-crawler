A program that analyzes HTML and finds a specific element, even after changes, using a set of extracted attributes. 

## Features

- Make [http](http://nodejs.org/api/http.html) to load provided HTML pages
- Compare elements properties and found sample element by relevant atrribute
- Show path of the sample elemnt if found


To run this program, first clone this repo and go to repo folder:
```bash
$ git clone git@bitbucket.org:piterson/agileengine-html-crawler.git
$ cd agileengine-html-crawler
```
Then install dependencies:
```bash
$ yarn
```
And run:
```bash
$ node src/analyse-html.js <url-original> <url-sample> <?source-element-id>
```

To view help information:
```bash
$ node src/analyse-html.js help
```

As arguments it takes two URLs of the original HTML page and the sample HTML page
Also there is an optinal parameter `source-element-id`, if it not set the program will find default element with `id="make-everything-ok-button"`

## Examples

### 1 case
```bash
$ node src/analyse-html.js https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-0-origin.html https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-1-evil-gemini.html
```
- output
```
Successfully found sample element. Element Text: 
                              Make everything OK
                            
Path to the sample element is:
HTML > BODY > DIV[wrapper] > DIV[page-wrapper] > DIV > DIV > DIV > DIV > A
```

### 2 case
```bash
$ node src/analyse-html.js https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-0-origin.html https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-2-container-and-clone.html
```
- output
```
Successfully found sample element. Element Text: 
                              Make everything OK
                          
Path to the sample element is:
HTML > BODY > DIV[wrapper] > DIV[page-wrapper] > DIV > DIV > DIV > DIV > DIV > A
```
### 3 case
```bash
$ node src/analyse-html.js https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-0-origin.html https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-3-the-escape.html
```
- output
```
Successfully found sample element. Element Text: 
                            Do anything perfect
                          
Path to the sample element is:
HTML > BODY > DIV[wrapper] > DIV[page-wrapper] > DIV > DIV > DIV > DIV > DIV > A
```
### 4 case
```bash
$ node src/analyse-html.js https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-0-origin.html https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-4-the-mash.html
```
- output
```
Successfully found sample element. Element Text: 
                            Do all GREAT
                          
Path to the sample element is:
HTML > BODY > DIV[wrapper] > DIV[page-wrapper] > DIV > DIV > DIV > DIV > A
```