const { JSDOM } = require('jsdom');
const axios = require('axios');
const process = require('process');

let targetElementId = "make-everything-ok-button";

if(process.argv.length < 4 || process.argv[2] === 'help') {
    console.log('to use this tool, please run:');
    console.log('node analyse-html.js <url-original> <url-sample> <?source-element-id>');
    console.log('if <source-element-id> is not provided then it will "make-everything-ok-button" by default');
    console.log('to see this message again run "node analyse-html.js help"');
    process.exit(process.argv.length !== 2);
}

const originalUrl = process.argv[2];
const sampleUrl = process.argv[3];
if (process.argv[4]) {
    targetElementId = process.argv[4];
}

function loadDOM(url) {
    return axios.get(url)
        .then(response => response.data)
        .then(body => new JSDOM(body));
}

async function main() {
    let originalDOM;
    let sampleDOM;
    try{
        originalDOM = await loadDOM(originalUrl);
        sampleDOM = await loadDOM(sampleUrl);
    } catch (err) {
        console.error('Error fetching provided URLs');
        process.exit(1);
    }

    const sourceEl = originalDOM.window.document.getElementById(targetElementId)
    if (!sourceEl) {
        console.error('Source element not found :(');
        process.exit(1);
    }
    console.log(`Successfully found source element. Element Text: ${sourceEl.textContent}`);
    const attrs = getNodeAttrs(sourceEl);
    
    let sampleEl
    for(const attr of ['id', 'onclick', 'href']) {
        console.log(`Trying to find by attr: ${attr}`);
        sampleEl = sampleDOM.window.document.querySelector(`[${attr}="${attrs[attr]}"]`);
        if (!sampleEl) {
            continue;
        }
        console.log(`Successfully found sample element. Element Text: ${sampleEl.textContent}`);
        break;
    }
    if (!sampleEl) {
        console.error('Sample element is not found :(');
        process.exit(1);
    }
    const path = [sampleEl];
    for (let curEl = sampleEl.parentNode; curEl !== sampleDOM.window.document; curEl = curEl.parentNode) {
        path.unshift(curEl);
    }
    console.log('Path to the sample element is:')
    console.log(path.map(n => nodeToString(n)).join(' > '));
    process.exit(0);
}

function getNodeAttrs(node) {
    const arr = Array.prototype.slice.apply(node.attributes);
    return arr.reduce((r, attr) => (r[attr.name] = attr.value, r), {});
}

function nodeToString(node) {
    const name = node.nodeName;
    const attrs = getNodeAttrs(node);
    if (attrs.id) {
        return `${name}[${attrs.id}]`;
    }
    return name;
}

main();
